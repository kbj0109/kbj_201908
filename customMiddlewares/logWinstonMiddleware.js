const { createLogger, transports, format } = require("winston");
const winstonDaily = require("winston-daily-rotate-file");

// Console에 로그 찍히는 형식
var consoleFormat = format.combine(
  format.colorize(),
  format.timestamp({
    format: "YYYY-MM-DD HH:mm:ss"
  }),
  format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
);

// File에 로그 찍히는 형식
var fileFormat = format.combine(
  format.timestamp({
    format: "YYYY-MM-DD HH:mm:ss"
  }),
  format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
);

// 기록에 쓰이는 logger 설정
var logger = new createLogger({
  transports: [
    // 콘솔 출력
    new transports.Console({
      level: "debug",
      format: consoleFormat
    }),
    // 로그 파일 설정
    new winstonDaily({
      level: "info", // 위의 7개 레벨 중 어떤 것에 쓰이는 설정인가
      filename: "./logs/%DATE%.log", //로그의 경로
      datePattern: "YYYY-MM-DD", //로그의 날짜와 파일이름. filename과 datePattern 합쳐져서 파일 이름이 됨
      format: fileFormat,
      maxsize: "20m", //20 MB,
      maxFiles: "14d" // 14일 간 보관
      //zippedArchive: true // 압축 여부
    })
  ]
});

// morgan과 결합되는 stream
logger.stream = {
  write: message => {
    logger.info(message);
  }
};

module.exports = logger;
