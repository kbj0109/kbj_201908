const routeMap = require("../routeMap");

//로그인이 안된 상태에서, 로그인 해야 사용 가능한 페이지에 들어가려고 한다면, 로그인 페이지로 이동
const OnlyForAuthorized = (req, res, next) => {
  if (!req.user) {
    res.redirect(routeMap.User + routeMap.Login);
  }
  next();
};

//로그인 된 상태에서, 로그인 안해야 사용 가능한 페이지에 들어가려고 한다면, 로그아웃 시키고 그 페이지로.
const OnlyForNotAuthorized = (req, res, next) => {
  if (req.user) {
    req.session.destroy();
    res.locals.loginUser = null;
  }
  next();
};

const basicMiddleware = (req, res, next) => {
  res.locals.pageTitle = "Jay Kim's Portfolio";
  res.locals.routeMap = routeMap;
  res.locals.loginUser = req.user || null; //req에 user 값이 있으면 배정, 아니면 null - (postJoin에서 로그인 될때 passport가 req.user를 생성)
  next(); //Custom Middleware에서는 기본적으로 next를 해줘야 다음 Middle를 거쳐서 다음 단계로 갈 수 있다.
};

module.exports = { OnlyForAuthorized, OnlyForNotAuthorized, basicMiddleware };
