const multer = require("multer"); //파일 Upload를 위한 모듈
const randomstring = require("randomstring"); //파일 업로드를 위한, Random String 값을 생성하는 모듈
const fs = require("fs"); // 파일 업로드를, 날짜 별로 폴더를 생성하여 관리하기 위해

// 파일 Upload 때 파일 저장소를 개발 중인 컴퓨텅니 로컬에 하기 위한 설정
var multerConfigStorage = multer.diskStorage({
  destination: function(req, file, cb) {
    var uploadedPath = "uploads/" + getToday(); // 파일을 저장할 때, 날짜별로 분류한다
    if (!fs.existsSync("./" + uploadedPath)) {
      fs.mkdirSync("./" + uploadedPath); //현재 날짜 이름의 폴더가 upload 경로에 존재하는지 확인하고 없으면 생성
    }
    cb(null, uploadedPath); //파일이 Upload되는 경로를 동적으로 설정
  },
  //실제 저장되는 파일명 설정
  filename: function(req, file, cb) {
    //Multer는 어떠한 파일 확장자도 추가하지 않습니다. //사용자 함수는 파일 확장자를 온전히 포함한 파일명을 반환해야 합니다.
    const randomKey = randomstring.generate({ length: 7 }); // 7자리의 random string을 생성
    file.addedRandomKey = randomKey; // upload되는 파일 정보를 담는 file 객체에 addedRandomKey 속성 추가
    cb(null, `${randomKey}_${file.originalname}`); //첨부된 파일을 저장할 때 randomKey와 함께 저장
  },
  limits: { filesize: 1024 * 1024 } // 1 MB 로 파일 사이즈 제한
});

// 현재 날짜를 yyyy_mm_dd 형식으로 반환
const getToday = () => {
  var today = new Date();
  var year = today.getFullYear();
  var month = ("0" + (today.getMonth() + 1)).substr(-2);
  var date = ("0" + today.getDate()).substr(-2);
  return `${year}_${month}_${date}`;
};

//
//
// AWS S3에 파일 첨부를 시작해보자
const multerS3 = require("multer-s3");
const aws = require("aws-sdk");

// S3의 연결 정보
const s3 = new aws.S3({
  secretAccessKey: process.env.AWS_SecretAccessKey,
  accessKeyId: process.env.AWS_AccesskeyID,
  region: "ap-northeast-2"
});

// 배포 환경인지, 개발 환경인지 구분하여 파일 첨부 Upload 장소 설정 변경
const getStorageSetting = () => {
  if (process.env.NodeEnv === "production") {
    //배포 환경에서는, 파일을 첨부할 때 AWS S3 저장소에 파일이 첨부되게
    return {
      storage: multerS3({
        s3: s3,
        acl: "public-read", //Access Control List
        bucket: `${process.env.AWS_S3_Bucket}/uploadedFiles` // S3에 설정된 저장소 kbj201908/uploadedFiles
      })
    };
  } else {
    //개발 환경에서는, 파일을 첨부할 때 개발 중인 컴퓨터 로컬에 저장되게
    return {
      storage: multerConfigStorage
    };
  }
};

const uploadFiles = multer(getStorageSetting());

module.exports = { s3, uploadFiles };
