// Board Write 페이지에서, 파일 첨부 테스트를 위한, 샘플 파일을 생성하고 다운로드
function createSampleFile() {
  //다운로드 되는 파일의 이름과 내용
  var currentTime = getCurrentTime();
  var data = "Downloaded at " + currentTime;
  var filename = currentTime + ".txt";
  var type = "text/plain";

  var file = new Blob([data], { type: type });
  if (window.navigator.msSaveOrOpenBlob)
    // IE10+
    window.navigator.msSaveOrOpenBlob(file, filename);
  else {
    // Others
    var a = document.createElement("a"),
      url = URL.createObjectURL(file);
    a.href = url;
    a.download = filename;
    document.body.appendChild(a);
    a.click();
    setTimeout(function() {
      document.body.removeChild(a);
      window.URL.revokeObjectURL(url);
    }, 0);
  }
}

//현재 시간을 millisecond 단위로 구해준다
const getCurrentTime = () => {
  var now = new Date();

  var year = now.getFullYear();
  var month = ("0" + (now.getMonth() + 1)).substr(-2);
  var date = ("0" + now.getDate()).substr(-2);
  var hour = ("0" + now.getHours()).substr(-2);
  var minute = ("0" + now.getMinutes()).substr(-2);
  var second = ("0" + now.getSeconds()).substr(-2);
  var millisecond = ("00" + now.getMilliseconds()).substr(-3);

  return `${year}-${month}-${date}_${hour}.${minute}.${second}.${millisecond}`;
};

// input file 태그의 규칙 확인
document.getElementById("file").onchange = function() {
  var fileList = [...document.getElementById("file").files];

  //파일 갯수 제한
  if (fileList.length > 5) {
    alert("파일은 5개까지만 선택 부탁 드립니다");
    document.getElementById("file").value = "";
    return;
  }
  // 파일 용량 제한
  fileList.map(element => {
    if (element.size > 1024 * 1024) {
      alert("첨부 파일 사이즈는 개당 1 MB로 제한하겠습니다 ㅠ");
      document.getElementById("file").value = "";
      return;
    }
  });
};
