//페이지 열릴 때 실행됨
window.onload = function() {
  callCommentList(); // Comment 리스트 호출
};

// Comment에서 작성 버튼 눌리면 동작
const requestWriteComment = async () => {
  const boardId = window.location.href.split("board/")[1]; //현재 URI를 잘라서 현재 페이지의 글 ID를 추출
  const commentTag = document.getElementById("comment"); // Comment의 내용을 담는 input text
  const response = await axios({
    //boardRead 파일에서 axios를 불러오기에 사용 가능한, API신호 보내고 결과 값 받는 Axios
    url: `/api/${boardId}/comment/write`, //req를 보내고 싶은 장소
    method: "POST", // 방식
    data: {
      comment: commentTag.value //전송할 데이터
    }
  });
  // 서버에서 성공 메세지가 등록되면, Comment List를 출력하고 input text 상자 초기화
  if (response.status === 200) {
    commentTag.value = "";
    callCommentList();
  }
  // 서버에서 Comment 작업이 실패하면, 실패 메세지 출력
  if (response.status === 400) {
    alert("Comment 입력이 실패했습니다. 잠시 후 다시 시도 해주세요");
  }
};

// Comment List를 서버에서 요청하여 받아온다.
const callCommentList = async () => {
  const boardId = window.location.href.split("board/")[1]; //현재 URI를 잘라서 현재 페이지의 글 ID를 추출
  const response = await axios({
    //boardRead 파일에서 axios를 불러오기에 사용 가능한, API신호 보내고 결과 값 받는 Axios
    url: `/api/${boardId}/comment`, //req를 보내고 싶은 장소
    method: "get" // 방식
  });
  // 서버에서 성공 메세지가 등록되면, Comment List를 출력하고 input text 상자 초기화
  if (response.status === 200) {
    insertAllComments(response.data);
  }
  // 서버에서 Comment 작업이 실패하면, 실패 메세지 출력
  if (response.status === 400) {
    alert("Comment 목록 불러오기에 실패했습니다.");
  }
};

// Comment List를 실제로 html 부분에 생성, 삽입
function insertAllComments(commentList) {
  const ulTag = document.getElementById("commentList");
  ulTag.innerHTML = ""; // Comment 목록 새로 생성할 때마다 초기화

  //Comment 리스트로 반복문
  for (var num = 0; num < commentList.length; num++) {
    const oneComment = commentList[num];
    var liTag = document.createElement("li");

    // Comment 작성자가 표시되는 부분
    var writerTag = document.createElement("span");
    writerTag.style.cssText = "font-weight: bold";
    writerTag.innerHTML = oneComment.writer.name;
    // Comment 작성 날짜가 표시되는 부분
    var writeDateTag = document.createElement("span");
    writeDateTag.style.cssText = "font-style: italic; color: gray;";
    writeDateTag.innerHTML = "  at " + DatePrettier(oneComment.writeDate);
    //결합
    liTag.innerHTML = `${writerTag.outerHTML} : ${oneComment.content} ${writeDateTag.outerHTML}`;
    ulTag.appendChild(liTag);
  }
}

//표시되는 날짜를 좀 더 보기좋게 변환
function DatePrettier(date) {
  var originalDate = new Date(date);
  var formatTime =
    originalDate.getFullYear() +
    "-" +
    (originalDate.getMonth() + 1) +
    "-" +
    originalDate.getDate() +
    " " +
    ("0" + originalDate.getHours()).substr(-2) +
    ":" +
    ("0" + originalDate.getMinutes()).substr(-2) +
    ":" +
    ("0" + originalDate.getSeconds()).substr(-2);
  return formatTime;
}

// Comment TextBox에서 Enter 키로 코멘트 입력 가능
function checkEnterKey(event) {
  if (event.keyCode == 13) {
    requestWriteComment();
  }
}
