function checkTranslateEnterKey(event) {
  if (event.keyCode == 13) {
    sendTranslateRequest();
  }
}

function sendTranslateRequest() {
  var text = document.getElementById("translateText").value.trim();

  $.ajax({
    url: "/api/translate",
    type: "get",
    data: { text },
    success: function(result) {
      document.getElementById("translatedResult").innerHTML = result;
    },
    error: function(error) {
      document.getElementById("translatedResult").innerHTML =
        "Sorry, Exception Happened on Translation";
    }
  });
}

function changeSampleMessage(code) {
  if (code == "kr") {
    document.getElementById("translateText").value =
      "김범준은 이 프로젝트에 많은 것을 해보려고 했습니다.";
  } else if (code == "jp") {
    document.getElementById("translateText").value = "ありがとうございます";
  } else {
    document.getElementById("translateText").value =
      "认识你很高兴. 最近过的好吗？";
  }
}
