var map = null; // Google Maps
var marker = null; // Google Map 위의 Marker
var geocoder = null; // Google Map에서 지역 검색할 때 사용되는 Geocoder

function initMap() {
  // Map의 기본 포지션은 김범준 집으로
  map = new google.maps.Map(document.getElementById("map"), {
    zoom: 18,
    center: { lat: 37.551039, lng: 126.977179 },
    gestureHandling: "greedy" // 사용자가 지도에서 마우스 휠로 Zoom 할때 Ctrl을 눌러야 하는가 아닌가 설정
  });
  marker = new google.maps.Marker({
    map,
    position: { lat: 37.551039, lng: 126.977179 }
  });
  geocoder = new google.maps.Geocoder();
  setToCurrentLocation();
}

// Client 의 현재 위치로 지도 위치 변경
function setToCurrentLocation() {
  navigator.geolocation.getCurrentPosition(function(clientLocation) {
    var position = {
      lat: clientLocation.coords.latitude,
      lng: clientLocation.coords.longitude
    };
    map.setCenter(position);
    marker.setPosition(position);
    updateWeather(
      clientLocation.coords.latitude,
      clientLocation.coords.longitude
    );
  });
}

//지역 이름으로 지도 위치 검색
function setMapByAddress() {
  var address = document.getElementById("address").value;
  if (address.trim() == "") return;

  geocoder.geocode({ address: address }, function(results, status) {
    if (status === "OK") {
      map.setCenter(results[0].geometry.location);
      marker.setPosition(results[0].geometry.location);
      updateWeather(
        results[0].geometry.location.lat(),
        results[0].geometry.location.lng()
      );
    } else {
      alert("Searching By Address Is Not Working : " + status);
      document.getElementById("weather").innerHTML = "";
    }
  });
}

function checkEnterKey(event) {
  if (event.keyCode == 13) {
    setMapByAddress();
  }
}

// 위도, 경도 정보를 바탕으로 날씨 정보를 받아와서 업데이트 한다
async function updateWeather(lat, lon) {
  document.getElementById("weather").innerHTML = ""; //날씨 정보 초기화

  try {
    const response = await axios({
      url: `/api/weather`,
      method: "post", // 사용자의 현재 위치는 노출하지 않게 전달
      data: {
        lat: lat,
        lon: lon
      }
    });

    if (response.status === 200) {
      var weatherInfo = response.data;

      var temp = document.createElement("h4");
      temp.innerHTML =
        "현재온도 : " + Math.round(weatherInfo.main.temp - 273.15);
      var desc = document.createElement("h4");
      desc.innerHTML = "상세날씨설명 : " + weatherInfo.weather[0].description;
      var city = document.createElement("h4");
      city.innerHTML = "도시이름  : " + weatherInfo.name;

      document.getElementById("weather").appendChild(temp);
      document.getElementById("weather").appendChild(desc);
      document.getElementById("weather").appendChild(city);
    } else {
      document.getElementById("weather").innerHTML =
        "- Failed to get Weather Information.";
    }
  } catch {
    document.getElementById("weather").innerHTML =
      "- Failed to get Weather Information.";
  }
}
