const mongoose = require("mongoose");

//DB 연결 정보는 일반적으로 보안되어야 하니까 dotenv로 해결
var dbAddress;
// 배포 환경에서는 clould.mongodb.com의 MongoDB kbj201908 저장소 DB로 연결하게 하자
if (process.env.NodeEnv === "production") {
  dbAddress = process.env.DB_ADDRESS_PROD;
} else {
  // 개발 환경에서는, 개발 중인 컴퓨터의 로컬 MongoDB에 연결하자
  dbAddress = process.env.DB_ADDRESS;
}

//DB 와 연결
mongoose.connect(dbAddress, {
  useNewUrlParser: true, //db Option 여러가지 설정 가능
  useFindAndModify: false,
  useCreateIndex: true,
  useUnifiedTopology: true
});
const db = mongoose.connection;

//DB 연결을 성공했을 때와 실패 했을 때, 실행될 메서드 설정
const dbOpen = () => console.log("Connected to Database: " + dbAddress);
const dbError = error =>
  console.log(`Error Occured on DB Connection: ${error}`);

db.once("open", dbOpen);
db.on("error", dbError);

require("../models/User"); // Model에서 Users 테이블에 대한 스키마를 가져와서 실행한다
require("../models/Board"); //Model에서 Boards 테이블에 대한 스키마를 가져와서 실행한다
require("../models/Comment");
require("../models/File");
