const redis = require("redis");
const connectRedis = require("connect-redis"); // 세션 관리를 Redis로 하기 위한 미들웨어

const mongoose = require("mongoose");
const connectMongo = require("connect-mongo"); //세션 관리를 MongoDB로 하기위한 미들웨어

// expressSession 설정을 인자로 받아서, Session을 관리하기 위한 SessionStore를 설정하자
const getSessionStore = session => {
  // 배포 환경에서는 Session 관리를 RedisLab의 Redis로 관리하자, 30 MB 까지 무료로 제공
  if (process.env.NodeEnv === "production") {
    // Redis 연결 정보 설정
    var redisClinet = redis.createClient(
      process.env.RedisPort,
      process.env.RedisHost
    );
    // 설정된 Redis로 연결 시도
    var redisConnectionResult = redisClinet.auth(
      process.env.RedisPassword,
      err => {
        if (err)
          console.log(
            `Not Connected to Redis DB (${process.env.RedisHost}:${process.env.RedisPort}) - `,
            err
          );
      }
    );
    //Redis 연결 성공 알림
    if (redisConnectionResult) {
      console.log(
        `Connected To Redis DB for Handling Session - ${process.env.RedisHost}:${process.env.RedisPort}`
      );
    }

    const SessionStore = connectRedis(session); //로그인 된 사용자들에 관한 세션을 RedisLabs에 저장해서 관리
    return new SessionStore({ client: redisClinet });
  } else {
    // 개발 환경에서는, 기본값인 MemoryStore에 세션을 저장하지 말고, 개발 중인 컴퓨터의 로컬 MongoDB에 세션을 저장하자
    const SessionStore = connectMongo(session); //로그인 된 사용자들에 관한 세션을 MongoDB에 저장해서 관리하자
    console.log("Connected To Local MongoDB for Handling Session");
    return new SessionStore({ mongooseConnection: mongoose.connection }); // 기본값은 MemoryStore, 이것을 MongoDB로 변경하고, MongoDB 연결정보를 넣어주자
  }
};

module.exports = { getSessionStore };
