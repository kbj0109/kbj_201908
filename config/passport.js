const passport = require("passport");
const User = require("../models/User");
const passportKakao = require("passport-kakao"); //카카오 회원 인증을 위한 모듈
const passportNaver = require("passport-naver");
const {
  kakaoLogin,
  kakaoJoin,
  naverLogin,
  naverJoin
} = require("../controller/userController");
const routeMap = require("../routeMap");

// createStrategy()는 passport-local 모듈에서 나오는, LocalStrategy 인스턴스를 편하게 받아오는 방법
passport.use(User.createStrategy()); //(passport-local-mongoose에서 나옴)

passport.serializeUser(User.serializeUser()); // passport-local에서 나옴, 실제로 세션을 메모리에 저장한다 (passport-local-mongoose에서 나옴)
passport.deserializeUser(User.deserializeUser()); // 저장된 세션을 불러와서 DB 정보와 조회해서 확인하고 불러오기

// passport를 이용한 카카오 인증을 위한 Strategy
const KakaoStrategy = passportKakao.Strategy;
passport.use(
  new KakaoStrategy(
    {
      clientID: process.env.KakaoRESTAPIKey, // 카카오 dev 사이트의 App REST API Key
      clientSecret: process.env.KakaoClientSecret, // 보안 강화를 위해 추가한 Client Secret, 필수는 아니다
      callbackURL: routeMap.User + routeMap.Kakao + routeMap.Callback
    },
    async (accessToken, refreshToken, profile, done) => {
      try {
        const exUser = await kakaoLogin(profile); // 카카오로 인증된 회원이 이전에 등록된 회원인지 확인
        if (exUser) {
          done(null, exUser); //맞으면 그냥 로그인
        } else {
          const newUser = await kakaoJoin(profile); //아니라면 회원가입 후 로그인;
          done(null, newUser);
        }
      } catch (error) {
        console.log("Kakao Strategy에서 Error 발생 - ", error);
        done(error);
      }
    }
  )
);

// passport를 이용한 네이버 인증을 위한 Strategy
const NaverStrategy = passportNaver.Strategy;
passport.use(
  new NaverStrategy(
    {
      clientID: process.env.NaverRESTAPIKey, // 네이버 dev 사이트의 App REST API Key
      clientSecret: process.env.NaverClientSecret, // 보안 강화를 위해 추가한 Client Secret, 필수는 아니다
      callbackURL: routeMap.User + routeMap.Naver + routeMap.Callback
    },
    async (accessToken, refreshToken, profile, done) => {
      try {
        const exUser = await naverLogin(profile); // 네이버로 인증된 회원이 이전에 등록된 회원인지 확인
        if (exUser) {
          done(null, exUser); //맞으면 그냥 로그인
        } else {
          const newUser = await naverJoin(profile); //아니라면 회원가입 후 로그인;
          done(null, newUser);
        }
      } catch (error) {
        console.log("Naver Strategy에서 Error 발생 - ", error);
        done(error);
      }
    }
  )
);
