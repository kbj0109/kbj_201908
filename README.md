# Jay Kim's First Project with NodeJS.

### Visit My Project via http://54.180.95.93:1001/

### This project will be composed by

- Used Express 4.17 for Web Framework
- AWS for Server
- PM2 for Server Managing
- MongoDB for Database
- Redis for Session Managing



##### 배포 환경시, 서버의 모든 구성이 Clould에 되어 있습니다.
##### 무료 환경만을 사용하다보니, Seoul Region이 아니라서 조금 느릴 수 있습니다.


### 배포 환경
- AWS - EC2에 pm2를 사용한 환경 구성 및 배포
- MongoDB Atals를 사용하여 Clould DB 환경 구성
- RedisLab을 사용한 Clould Session 관리 환경 구성
- AWS S3 Storage에 파일 Upload/Download/Delete 가능

### 개발 환경
- 개발 중인 컴퓨터 localhost:1001에 배포
- 로컬 MongoDB 사용하여 DB 관리
- 로컬 MongoDB를 사용한 Session 관리
- 로컬 uploads 폴더 내 파일 Upload/Download/Delete 가능

### 구현 요소
- Express 4.17에 기반한 Restful 서버 구현
- 기본적인 게시판 및 코멘트 기능
- multer를 사용한 파일 다중 첨부, 다운로드 기능
- passport를 사용한 일반회원, 카카오, 네이버 인증 기능 
- Google Maps API 를 사용한 지도 기능
- OpenWeatherMap API 를 사용한 날씨 탐지 기능
- GCP의 Google Translate API를 사용한 번역 기능
- Winston을 사용한 로그 기록
- AJAX 방식으로 구현한 여러가지 기능
- Flash를 사용한 일회성 안내 메세지 구현
                