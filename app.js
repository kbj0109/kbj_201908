const express = require("express");
const morgan = require("morgan"); // 로그를 기록하게 하는 미들웨어
const helmet = require("helmet"); // 보안 향상을 위한 미들웨어
const routeMap = require("./routeMap");
const globalRouter = require("./routers/globalRouter");
const userRouter = require("./routers/userRouter");
const boardRouter = require("./routers/boardRouter");
const apiRouter = require("./routers/apiRouter");
const cookieParser = require("cookie-parser"); // 쿠키 정보를 저장/추출, 그리고 암호화 등을 할 수 있는 미들웨어
const bodyParser = require("body-parser"); //post 형식에서 클라이언트로부터 데이터를 받아오기 위한, req에 데이터를 실어오는 미들웨어
const session = require("express-session"); //클라이언트의 쿠키에 랜덤값을 배정해서, 세션을 관리할 수 있게 해주는 미들웨어
const passport = require("passport");
const { basicMiddleware } = require("./customMiddlewares/middleware"); // 개인적으로 만든 Middleware
const logger = require("./customMiddlewares/logWinstonMiddleware");
const connectFlash = require("connect-flash"); // flash를 사용해 일회성 메세지를 나타내기 위한 미들웨어

require("./config/passport");
const { getSessionStore } = require("./config/sessionHandling");

const app = express(); //서버 실행을 준비, index.js의 listen에서 서버가 실행된다

app.use(express.static("views")); //정적 파일 접근을 위한 경로 지정
app.use("/static", express.static("static")); // 정적인 파일을 참조시키기 위함.

app.use(helmet()); //서버 보안 업그레이드
//app.use(morgan("dev")); // combined, common, 등의 여러 방식 중, dev 선택
app.use(morgan("combined", { stream: logger.stream })); // 로그 기록을 위해 morgan과 winston 결합
app.set("view engine", "pug"); // view engine을 pug 혹은 ejs로 설정 가능한데, 나는 pug를 사용

app.use(bodyParser.json()); //post 형식으로 받는 클라이언트로부터의 정보를 json 형식으로 req.body에 할당하는 미들웨어 실행
app.use(bodyParser.urlencoded({ extended: true })); // 중첩된 객체 표현을 허용할 것인가, 객체 안에 객체를 파싱할 수 있게 하려면 true
app.use(cookieParser(process.env.COOKIE_PASSCODE)); // PASSCODE를 기준으로 암호화 가능한 쿠키를 req에 저장 가능하게

app.use(
  session({
    // 어떤 클라이언트에서 들어오는 request인지 확인하기 위해, req의 쿠키에 랜덤값을 저장하게 함.
    secret: process.env.COOKIE_PASSCODE, // 사용자에게 저장시키는 쿠키 정보의 암호 코드,
    resave: false, //매 순간 세션을 재 저장 할 것인가
    saveUninitialized: false,
    store: getSessionStore(session)
  })
);
app.use(connectFlash()); // Cookie-Parser과 Express-Session 뒤에 위치하게 하자

//passport 관련 사항은 cookie-parser과 body-parser, express-session 뒤에 넣어주자
app.use(passport.initialize()); // passport를 실제로 구동한다, passport가 구동되면서 전달받은 쿠키를 뒤져, 쿠키에 해당하는 사용자를 찾아준다.
app.use(passport.session()); // passport에서 session을 사용하도록 설정

app.use(basicMiddleware);

app.use(routeMap.Home, globalRouter);
app.use(routeMap.User, userRouter);
app.use(routeMap.Board, boardRouter);
app.use(routeMap.Api, apiRouter);

// Uncaught Exception 발생시, 로그에 기록되게
app.use((err, req, res, next) => {
  console.log(err); //에러 메세지 잘 나타남
  next();
});

module.exports = app;
