// global Router - 로그인 되지 않은 상태에서 입장 될 페이지
const HOME = "/";
const INTRO = "/intro";

// user Router - User 관련된 페이지
const USER = "/users";
const JOIN = "/join";
const LOGIN = "/login";
const LOGOUT = "/logout";

// board Router - Board (게시판) 관련 페이지
const BOARD = "/board";
const WRITE = "/write";
const READ = "/:id"; //고정된 값을 보내는게 아닌, 동적으로 값을 지정해서 사용할 때.
const EDIT = "/:id/edit";
const DELETE = "/:id/delete";

// api Router
const COMMENT = "/:id/comment";
const WRITECOMMENT = "/:id/comment/write"; // comment Router - Board 게시판 글에 Comment 기능 추가

const DOWNLOAD = "/:id/download";

// API Router - API 관련 기능
const API = "/api";
const MAP = "/map";
const WEATHER = "/weather";
const TRANSLATE = "/translate";

// SNS 인증 관련
const AUTH = "/auth";
const CALLBACK = "/callback";
const KAKAO = "/kakao";
const NAVER = "/naver";

const routeMap = {
  Home: HOME,
  User: USER,
  Join: JOIN,
  Login: LOGIN,
  Logout: LOGOUT,
  Board: BOARD,
  Write: WRITE,
  Read: id => {
    //Board의 _id를 인자로 받는 Read 메서드 실행,
    if (id) {
      return "/" + id; // '/id 형식을 리턴
    } else {
      return READ;
    }
  },
  Edit: id => {
    if (id) {
      return "/" + id + "/edit";
    } else {
      return EDIT;
    }
  },
  Delete: id => {
    if (id) {
      return "/" + id + "/delete";
    } else {
      return DELETE;
    }
  },
  Api: API,
  Comment: COMMENT,
  WriteComment: WRITECOMMENT, //"/:id/comment"
  Download: id => {
    if (id) {
      return "/" + id + "/download";
    } else {
      return DOWNLOAD;
    }
  },
  Auth: AUTH,
  Callback: CALLBACK,
  Kakao: KAKAO,
  Naver: NAVER,
  Map: MAP,
  Weather: WEATHER,
  Translate: TRANSLATE,
  Intro: INTRO
};

module.exports = routeMap;
