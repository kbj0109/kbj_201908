const mongoose = require("mongoose");
const passportLocalMongoose = require("passport-local-mongoose"); // Mongoose의 플러그인, 패스포트로 로컬에서 로그인 하는 것을 쉽게. 그리고 사용되는 password 암호화

const UserSchema = new mongoose.Schema({
  email: { type: String, unique: true, required: true }, // Email을 Primary Key로 사용하자. Unique 속성 true
  name: { type: String, required: true },
  //password: { type: String, required: true }, //passport-local-mongoose의 User.register를 사용하면서, password 컬럼 제거
  joinDate: { type: Date, default: Date.now },
  boards: [{ type: mongoose.Schema.Types.ObjectId, ref: "Board" }], // Board 테이블의 _id 컬럼과 연결, 한 사람이 여러 글을 작성 가능하기에 배열로 지정
  comments: [{ type: mongoose.Schema.Types.ObjectId, ref: "Comment" }], // Comment 테이블과 연결
  snsType: { type: String, enum: [null, "Kakao", "Naver"], default: null }, // SNS 인증으로 회원가입 된 경우. 기본값은 null, 입력 가능한 값 제한
  snsId: { type: String, default: null } //SNS 인증으로 회원가입 된 경우, SNS 서버에서 받아온 아이디 값이 들어간다
});

UserSchema.plugin(passportLocalMongoose, {
  usernameField: "email" // 무엇으로 인증을 할 것인가, usernameField의 디폴트는 'username'
  //passwordField: "password" // passwordField는 'password'
});

const User = mongoose.model("User", UserSchema);

module.exports = User;
