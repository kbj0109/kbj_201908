const mongoose = require("mongoose");

const BoardSchema = new mongoose.Schema({
  title: { type: String },
  content: { type: String },
  writeDate: { type: Date, default: Date.now },
  writer: {
    // USER 테이블의 _id 컬럼과 연결,
    type: mongoose.Schema.Types.ObjectId, //들어가는 값이 특정 다큐먼트의 _id라는 것
    ref: "User" //이 컬럼이 참조하는 값이 User 테이블에서 나온다는 것
  },
  files: [
    //첨부된 파일들 ID
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "File"
    }
  ],
  comments: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Comment"
    }
  ]
});

const Board = mongoose.model("Board", BoardSchema);

module.exports = Board;
