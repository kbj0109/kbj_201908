const mongoose = require("mongoose");

const CommentSchema = new mongoose.Schema({
  board: {
    //해당 코멘트가 Board의 어떤 글에 속해 있는지
    type: mongoose.Schema.Types.ObjectId,
    ref: "Board"
  },
  content: { type: String },
  writeDate: { type: Date, default: Date.now },
  writer: {
    // USER 테이블의 _id 컬럼과 연결
    type: mongoose.Schema.Types.ObjectId, //들어가는 값이 특정 다큐먼트의 _id라는 것
    ref: "User" //이 컬럼이 참조하는 값이 User 테이블에서 나온다는 것
  }
});

const Comment = mongoose.model("Comment", CommentSchema);

module.exports = Comment;
