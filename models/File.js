const mongoose = require("mongoose");

const FileSchema = new mongoose.Schema({
  board: {
    //파일이 속한 Board 게시판의 글 ID를 저장
    type: mongoose.Schema.Types.ObjectId,
    ref: "Board"
  },
  storedFilePath: { type: String }, // 파일이 서버 어디에 저장되어 있는지
  originalFileName: { type: String } //파일이 첨부될 때의 원래 이름
});

const File = mongoose.model("File", FileSchema);

module.exports = File;
