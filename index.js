require("dotenv").config();
//다른 파일보다 가장먼저 dotenv.config()를 실행해야, env 파일의 기밀정보를 읽어와서 사용할 수 있다.

require("./config/db");
const app = require("./app");

const PORT = process.env.PORT;

app.listen(PORT, () => {
  console.log(`Server Is Ready with http://localhost:${PORT}`);
});
