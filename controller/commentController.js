const Board = require("../models/Board");
const Comment = require("../models/Comment");

// Board 게시판의 특정 글에 대한 Comment 리스트를 가져온다
const getCommentList = async (req, res) => {
  try {
    const commentList = await Comment.find({ board: req.params.id }) //Board 글 id에 속한 Comment 리스트 찾기
      .populate("writer", "name email") //작성자의 id와 이름, 이메일 주소를 연결
      .sort({ writeDate: 1 }); //최신 Comment를 아래로 정렬
    res.status(200).json(commentList); // 결과값 성공으로 저장하고, 리스트를 함께 돌려준다
  } catch (error) {
    console.log("getCommentList에서 Exception - ", error);
    res.status(400);
  } finally {
    res.end(); //사용자 요청 종료
  }
};

// Comment를 Board 게시판 글에 추가
const postWriteComment = async (req, res) => {
  try {
    // 1. Comment 쓰려는 Board 게시판 글을 찾는다.
    const oneBoard = await Board.findById(req.params.id);
    // 2. Comment 를 DB에 생성, 저장 한다
    const newComment = await Comment.create({
      // Comment 테이블에 Comment 저장
      board: req.params.id, // Comment 가 속한 Board 게시판의 글 ID
      content: req.body.comment, // Comment 내용
      writer: req.user.id // Comment 작성자 ID
    });
    // 3. 해당 Board 게시판 글에 속한 Comment 배열을 추가, 저장한다.
    oneBoard.comments.push(newComment.id);
    oneBoard.save();
    // 4. User 테이블에 작성된 Comment ID 저장
    req.user.comments.push(newComment.id);
    req.user.save();
    // 마무리
    res.status(200); // 모든 필요 동작이 성공했다는 상태를 알림
  } catch (error) {
    console.log("postWriteComment에서 Exception - ", error);
    res.status(400);
  } finally {
    res.end(); //사용자 요청 종료
  }
};

module.exports = { getCommentList, postWriteComment };
