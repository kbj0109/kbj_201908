const Board = require("../models/Board");
const File = require("../models/File");
const axios = require("axios");
const { Translate } = require("@google-cloud/translate"); //Google Cloud Translate 모듈에서 Tranlate 멤버 하나만 가져오기

//가장 기본인 Home 페이지로 이동
const getHome = async (req, res) => {
  try {
    const boards = await Board.find({})
      .sort({ _id: -1 }) // _id 컬럼으로 정렬, -1은 Ascending, 1은 Descending
      .populate("writer", "name"); // Board 컬렉션의 writer 컬럼을 populate 하는데, ref 된 User 컬렉션의 name만 가져오게

    if (!req.user) {
      req.flash(
        "info",
        "로그인 하셔야 글 쓸 수 있어요.  - guest@gmail.com / guest 로 로그인 가능합니다"
      );
    }
    res.render("home", {
      boards,
      successMessage: req.flash("success"),
      infoMessage: req.flash("info"),
      errorMessage: req.flash("error")
    });
  } catch (error) {
    res.render("home", { errorMessage: "Board List 불러오기 오류" });
  }
};

// Board 게시판에 첨부된 파일 다운로드 (개발 환경일 때, 첨부 파일이 로컬 컴퓨터에 저장될 때만 사용 가능했던 방식)
const getFileDownloadOriginal = async (req, res) => {
  try {
    const downloadFile = await File.findById(req.params.id); // File ID를 이용해 먼저 파일 정보를 찾는다
    //파일 정보가 있다면 다운로드 실행
    if (downloadFile) {
      res.download(
        downloadFile.storedFilePath, // 파일이 저장되어 있는 서버의 경로
        downloadFile.originalFileName, // 파일이 다운로드 되는 Original 이름
        // 파일 다운로드에 에러 발생시, downloadError에 에러가 담겨진다
        downloadError => {
          if (downloadError) {
            console.log("File Download Error - ", downloadError);
            res.status(downloadError.statusCode); //파일 다운로드의 결과 상태를 res에 저장
            res.end(); // res 종료
          }
        }
      );
    } else {
      res.status(404); //파일에 대한 정보가 제대로 확인되지 않을시, 404 Error
      res.end();
    }
  } catch (error) {
    console.log("getFileDownload에서 Exception - ", error);
    res.end();
  }
};

// Board 게시판에 첨부된 파일 다운로드 (AWS S3)
const request = require("request");
const mime = require("mime");
const getFileDownload = async (req, res) => {
  try {
    const downloadFile = await File.findById(req.params.id); // File ID를 이용해 먼저 파일 정보를 찾는다
    //파일 정보가 있다면 다운로드 실행
    if (downloadFile) {
      const fileType = mime.getType(downloadFile.originalFileName); // 파일의 Type을 정의 img, video 등
      res.setHeader(
        "content-disposition",
        "attachment; filename=" +
          setEncodeFilename(req, downloadFile.originalFileName) // 사용자가 받게 되는 파일의 이름
        // 다운 받는 파일의 이름에 한글이 포함된 경우가 있을 수 있기에, setEncodeFilename으로 Encoding 변환
      );
      res.setHeader("content-type", fileType);
      const stream = await request(downloadFile.storedFilePath); //파일이 저장된 경로에서 stream을 읽어온다
      stream.pipe(res); // 사용자 다운로드 시작
    } else {
      res.status(404); //파일에 대한 정보가 제대로 확인되지 않을시, 404 Error
      res.end();
    }
  } catch (error) {
    console.log("getFileDownload에서 Exception - ", error);
    res.end();
  }
};

// 한글 파일을 다운로드 받기 위해서는 이 부분이 필요
const iconvLite = require("iconv-lite");
const setEncodeFilename = (req, filename) => {
  var header = req.headers["user-agent"];
  if (header.includes("MSIE") || header.includes("Trident")) {
    return encodeURIComponent(filename).replace(/\\+/gi, "%20");
  } else if (header.includes("Chrome")) {
    return iconvLite.decode(iconvLite.encode(filename, "UTF-8"), "ISO-8859-1");
  } else if (header.includes("Opera")) {
    return iconvLite.decode(iconvLite.encode(filename, "UTF-8"), "ISO-8859-1");
  } else if (header.includes("Firefox")) {
    return iconvLite.decode(iconvLite.encode(filename, "UTF-8"), "ISO-8859-1");
  }
  return filename;
};

const getMap = (req, res) => {
  res.render("map");
};

// Client가 전달하는 위도와 경도를 이용해 해당 지역의 날씨 정보를 가져온다
const postGetWeather = async (req, res) => {
  try {
    var lat = req.body.lat;
    var lon = req.body.lon;

    const response = await axios({
      url: `http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=${process.env.WeatherAPIKey}`,
      method: "get"
    });
    res.status(200).json(response.data);
  } catch (err) {
    console.log("getWeather에서 Error 발생 - ", err);
    res.status(500);
  }
  res.end();
};

const translate = new Translate();
// Footer에서 GoogleTranslateAPI 사용 예제에서 받은 Text를 영어로 번역
const getTranslateText = async (req, res) => {
  try {
    const target = "en"; //영어로 번역
    const [translation] = await translate.translate(req.query.text, target); //여러가지 정보를 담은 객체를 가져오는데, 그 중 결과 값만.
    res.status(200).json(translation);
  } catch {
    res.status(500);
  }
  res.end();
};

const getIntro = (req, res) => {
  res.render("intro");
};

const getTranslate = (req, res) => {
  res.render("translate");
};

module.exports = {
  getHome,
  getFileDownloadOriginal,
  getFileDownload,
  getMap,
  postGetWeather,
  getTranslate,
  getTranslateText,
  getIntro
};
