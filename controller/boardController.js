const routeMap = require("../routeMap");
const Board = require("../models/Board");
const File = require("../models/File");
const fs = require("fs"); // 로컬에 첨부된 파일 삭제를 위한 모듈
const { s3 } = require("../customMiddlewares/uploadMiddleware");

// Board 게시판 글 쓰기 페이지로 이동
const getWrite = (req, res) => {
  res.render("boardWrite");
};

// Board 게시판 글 쓰기에서 데이터 저장
const postWrite = async (req, res) => {
  try {
    // 1. DB의 Board 테이블에 글 정보 저장
    const newBoard = await Board.create({
      title: req.body.title,
      content: req.body.content,
      writer: req.user.id
    });
    // 2. DB의 User 테이블에 글 연결 정보 저장
    req.user.boards.push(newBoard.id);
    req.user.save();

    // 3. DB의 File 테이블에 글에 첨부되는 파일 정보를 각각 저장
    var fileInfoArray = []; //한번에 여러 Document를 DB에 저장하기 위해, InsertMany 사용 준비 Array.
    for (var num = 0; num < req.files.length; num++) {
      const fileInfo = req.files[num]; // 하나의 파일 정보를 세분화하여 각 Array 요소에 담는다.

      // 배포/개발 환경에 따라 저장되는 값이 달라진다
      var storedFilePath;
      if (process.env.NodeEnv === "production") {
        storedFilePath = fileInfo.location; // AWS S3에 파일을 첨부 할 때 등록되는 URL
      } else {
        storedFilePath = fileInfo.path; // 파일 다운로드를 위한, 전체 경로
      }

      fileInfoArray.push({
        board: newBoard.id, //이 파일이 첨부된 글의 ID
        storedFilePath: storedFilePath, // 파일 다운로드를 위한 첨부 파일 위치
        originalFileName: fileInfo.originalname //다운로드 할 때, 원래 이름을 유지하기 위한 Original Name
      });
    }
    // 4. 준비된 File 정보 배열 Object를 이용해 DB에 실제로 저장
    const newFile = await File.insertMany(fileInfoArray, function(err, res) {
      if (err) {
        console.log("파일 첨부 중 Error - ", err);
      } else {
        // Exception 없이 File 테이블에 File 정보 저장이 성공했다면, 파일들 ID를 Board 데이터에 넣어준다
        res.forEach(one => {
          newBoard.files.push(one._id);
        });
        newBoard.save();
      }
    });

    res.redirect(routeMap.Board + routeMap.Read(newBoard.id));
  } catch (error) {
    console.log("postWrite에서 Exception - ", error);
    res.redirect(routeMap.Home);
  }
};

// Board 게시판 글 읽기
const getRead = async (req, res) => {
  console.log("req.params - ", req.params, "\n"); // req.params은 route의 parameter, board/글 id의 id 부분.
  console.log("req.body - ", req.body); // 빈 내용 - body-parser를 사용하면서, Form 형식으로 데이터가 넘어올 때 사용
  try {
    // Board 내용을 리스트로 받아올 때, Board의 writer 컬럼의 참조 컬렉션의 name, email 컬럼과 populate
    const readBoard = await Board.findById(req.params.id).populate(
      "writer",
      "name email"
    );
    const fileList = await File.find({ board: req.params.id }); //첨부된 파일이 있다면, 파일 리스트를 첨부해서 Read로 보낸다
    res.render("boardRead", { oneBoard: readBoard, fileList });
  } catch (error) {
    console.log("getRead Exception 발생 - ", error);
    res.redirect(routeMap.Home);
  }
};

// Board 게시판에서 특정 글의 수정 페이지로 이동
const getEdit = async (req, res) => {
  const boardToEdit = await Board.findById(req.params.id);
  res.render("boardEdit", { oneBoard: boardToEdit });
};

// Board 게시판에서 특정 글의 수정을 실제로 DB에 저장
const postEdit = async (req, res) => {
  try {
    //먼저 수정하려는 비디오의 작성자가, 현재 로그인된 사용자와 동일한지 확인
    const boardToEdit = await Board.findById(req.params.id);
    if (boardToEdit.writer != req.user.id) {
      throw new Error("사용자가 작성한 글이 아닙니다.");
    }
    //동일하면 수정
    await Board.findByIdAndUpdate(req.params.id, {
      title: req.body.title,
      content: req.body.content
    });
    res.redirect(routeMap.Board + routeMap.Read(req.params.id));
  } catch (error) {
    console.log("getDelete Exception 발생 - ", error);
    res.redirect(routeMap.Board + routeMap.Read(req.params.id));
  }
};

// Board 게시판 글 삭제
const getDelete = async (req, res) => {
  try {
    //먼저 지우려는 비디오의 작성자가, 현재 로그인된 사용자와 동일한지 확인
    const boardToDelete = await Board.findById(req.params.id);
    if (boardToDelete.writer != req.user.id) {
      throw new Error("사용자가 작성한 글이 아닙니다.");
    }

    // Board 게시판에서 글을 삭제
    await Board.findByIdAndRemove(req.params.id); //먼저 Board 테이블에서 글 삭제하고
    const boardIndex = req.user.boards.indexOf(req.params.id); // User 테이블에 연결된 Board 컬럼의 Array에서 삭제된 글의 위치를 찾는다.
    req.user.boards.splice(boardIndex, 1); //그리고 삭제된 글의 번호를 User 테이블의 Board Array에서도 제거
    req.user.save(); //저장

    // 서버에 저장된 첨부 파일들 중, 삭제 될 리스트를, DB에서 먼저 가져온다
    const deleteFileList = await File.find({ board: req.params.id });

    // 첨부된 File 들 정보를 File DB에서 삭제
    File.deleteMany({ board: req.params.id }, err => {
      if (err) console.log("delete Many에서 에러 발생 - ", err);
    });

    deleteFiles(deleteFileList); // 개발/배포 환경에 따라 첨부된 파일을 삭제
    res.redirect(routeMap.Home);
  } catch (error) {
    console.log("getDelete Exception 발생 - ", error);
    res.redirect(routeMap.Board + routeMap.Read(req.params.id));
  }
};

// 파일 정보를 리스트로 받아서, 개발/배포 환경에 따라 첨부된 파일을 삭제
const deleteFiles = (fileList = []) => {
  // AWS S3에 첨부된 파일 삭제하기 - 참조 https://gist.github.com/jeonghwan-kim/9597478
  if (process.env.NodeEnv === "production") {
    // 1. 삭제할 파일의 Key 값을 Object 배열로 정리
    var fileKeyList = fileList.map(element => {
      var fileKey = element.storedFilePath;
      fileKey = fileKey.substring(fileKey.indexOf("uploadedFiles"));
      return { Key: fileKey };
    });

    // 2. S3에 파일 삭제를 위해 다녀올 설정 정보 정리
    var params = {
      Bucket: process.env.AWS_S3_Bucket,
      Delete: { Objects: fileKeyList }
    };

    // 3. AWS S3에 삭제 요청
    s3.deleteObjects(params, function(err, data) {
      // 파일 삭제 중 에러 발생의 경우
      if (err) {
        console.log(err, err.stack);
      } else {
        console.log("삭제된 파일 리스트 - " + data); // 파일이 모두 성공적으로 삭제된 경우
      }
    });
  } else {
    // 개발 환경에서는, 첨부된 파일들이 로컬 개발 컴퓨터의 uploads 폴더에 저장되므로, 로컬에서 삭제
    fileList.forEach(oneFile => {
      fs.exists(oneFile.storedFilePath, checkExist => {
        if (checkExist) {
          fs.unlink(oneFile.storedFilePath, err => {
            if (err) console.log("파일 삭제 에러 발생 - ", err);
          });
        }
      });
    });
  }
};

module.exports = { getWrite, postWrite, getRead, getEdit, postEdit, getDelete };
