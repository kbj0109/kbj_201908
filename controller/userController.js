const routeMap = require("../routeMap");
const User = require("../models/User");
const passport = require("passport");

// Join 페이지로 이동하기
const getJoin = (req, res) => {
  res.render("userJoin", {
    successMessage: req.flash("success"),
    infoMessage: req.flash("info"),
    errorMessage: req.flash("error")
  });
};

// Join 페이지에서 실제로 회원가입 Join을 실행
const postJoin = async (req, res) => {
  try {
    // 입력된 비밀번호 2개가 일치하지 않는다면, Join 페이지로 되돌린다
    if (req.body.password !== req.body.passwordConfirm) {
      res.status(400);
      req.flash("error", "Please Insert Same Passwords.");
      res.redirect(routeMap.User + routeMap.Join);
    }
    // 이미 등록된 이메일인지 확인한다 (중복 확인)  //아래에 create가 아닌 register를 사용하면 중복 확인까지 해주지만, 일단 참고삼아 놔두자
    const userExist = await User.exists({ email: req.body.email });
    if (userExist) {
      req.flash("error", "Your Email Address Is Already Registered");
      res.redirect(routeMap.User + routeMap.Join);
    }
    // DB에 사용자를 등록. 보통 DB에 등록할 때는 create를 쓰지만, passport-local-mongoose가 제공하는 register를 쓰자.
    const user = await User({
      name: req.body.name, // User 테이블 각각의 컬럼에 값을 대입하여 삽입
      email: req.body.email
    });
    await User.register(user, req.body.password);
    req.flash("success", "You Successfully Joined. Please Login Now.");
    res.redirect(routeMap.Home);
  } catch (error) {
    console.log("Exception Occured at postJoin");
    console.log(error);
    res.redirect(routeMap.User + routeMap.Join);
  }
};

// Login 페이지로 이동
const getLogin = (req, res) => {
  res.render("userLogin", {
    successMessage: req.flash("success"),
    infoMessage: req.flash("info"),
    errorMessage: req.flash("error")
  });
};

// Login 페이지에서, 실제로 Login 하기
const postLogin = passport.authenticate("local", {
  failureRedirect: routeMap.User + routeMap.Login, //로그인 실패할 경우
  successRedirect: routeMap.Home, //로그인 성공할 경우
  failureFlash: "로그인 실패 했습니다.",
  successFlash: "로그인 성공 했습니다."
});

//Logout 실행
const logout = (req, res) => {
  req.flash("info", "로그아웃 되었습니다, 다시 만나요~");
  req.logout();
  //req.session.destroy();
  res.locals.loginUser = null;
  res.redirect(routeMap.User + routeMap.Login);
};

// 인증된 카카오 회원이 이전에 가입된 회원인지 확인
const kakaoLogin = async profile => {
  const exUser = await User.findOne({ email: profile._json.kaccount_email });
  return exUser;
};
//카카오로 인증된 회원이 등록되지 않은 회원이라면 회원가입 시키는 메서드
const kakaoJoin = async profile => {
  const newUser = await User.create({
    email: profile._json.kaccount_email,
    name: profile.displayName,
    snsType: "Kakao",
    snsId: profile.id
  });
  return newUser;
};
// 카카오 인증 완료 후, 이동 경로 설정
const kakaoAuthenticateCallBack = {
  successRedirect: routeMap.Home,
  failureRedirect: routeMap.User + routeMap.Login
};

// 인증된 네이버 회원이 이전에 가입된 회원인지 확인
const naverLogin = async profile => {
  const exUser = await User.findOne({ email: profile._json.email });
  return exUser;
};
// 네이버로 인증된 회원이 등록되지 않은 회원이라면 회원가입 시키는 메서드
const naverJoin = async profile => {
  const newUser = await User.create({
    email: profile._json.email,
    name: profile._json.nickname,
    snsType: "Naver",
    snsId: profile.id
  });
  return newUser;
};
// 네이버 인증 완료 후, 이동 경로 설정
const naverAuthenticateCallBack = {
  successRedirect: routeMap.Home,
  failureRedirect: routeMap.User + routeMap.Login
};

module.exports = {
  getJoin,
  postJoin,
  getLogin,
  postLogin,
  logout,
  kakaoLogin,
  kakaoAuthenticateCallBack,
  naverLogin,
  naverJoin,
  naverAuthenticateCallBack
};
