const express = require("express");
const routeMap = require("../routeMap");
const {
  getHome,
  getMap,
  getIntro,
  getTranslate
} = require("../controller/globalController");

const globalRouter = express.Router();

globalRouter.get(routeMap.Home, getHome);
globalRouter.get(routeMap.Map, getMap);
globalRouter.get(routeMap.Intro, getIntro);
globalRouter.get(routeMap.Translate, getTranslate);

module.exports = globalRouter;
