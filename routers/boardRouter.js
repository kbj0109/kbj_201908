const express = require("express");
const routeMap = require("../routeMap");
const {
  getWrite,
  getRead,
  getEdit,
  getDelete,
  postWrite,
  postEdit
} = require("../controller/boardController");
const { OnlyForAuthorized } = require("../customMiddlewares/middleware");
const { uploadFiles } = require("../customMiddlewares/uploadMiddleware");

const boardRouter = express.Router();

boardRouter.get(routeMap.Write, OnlyForAuthorized, getWrite);
boardRouter.post(
  routeMap.Write,
  OnlyForAuthorized,
  uploadFiles.array("file", 5), //Client 페이지의 input file 태그의 name이 file, 첨부 파일은 5개까지
  postWrite
);
boardRouter.get(routeMap.Read(), getRead); //Board 읽기에서는 routeMap이 고정되지 않고 메서드가 실행되기에, 받는 Router에서도 메서드로.
boardRouter.get(routeMap.Edit(), getEdit);
boardRouter.post(routeMap.Edit(), postEdit);
boardRouter.get(routeMap.Delete(), getDelete);

module.exports = boardRouter;
