const express = require("express");
const routeMap = require("../routeMap");
const {
  getJoin,
  getLogin,
  postJoin,
  postLogin,
  logout,
  kakaoAuthenticateCallBack,
  naverAuthenticateCallBack
} = require("../controller/userController");
const { OnlyForNotAuthorized } = require("../customMiddlewares/middleware");
const passport = require("passport");

const userRouter = express.Router();

userRouter.get(routeMap.Join, OnlyForNotAuthorized, getJoin);
userRouter.post(routeMap.Join, OnlyForNotAuthorized, postJoin);

userRouter.get(routeMap.Login, OnlyForNotAuthorized, getLogin);
userRouter.post(routeMap.Login, OnlyForNotAuthorized, postLogin);
userRouter.get(routeMap.Logout, logout);

// 카카오 인증 페이지로 이동하기
userRouter.get(
  routeMap.Kakao + routeMap.Auth,
  OnlyForNotAuthorized,
  passport.authenticate("kakao")
);
// 카카오 인증 후 돌아올 때
userRouter.get(
  routeMap.Kakao + routeMap.Callback,
  OnlyForNotAuthorized,
  passport.authenticate("kakao", kakaoAuthenticateCallBack)
);

// 네이버 인증 페이지로 이동하기
userRouter.get(
  routeMap.Naver + routeMap.Auth,
  OnlyForNotAuthorized,
  passport.authenticate("naver")
);
// 네이버 인증 후 돌아올 때
userRouter.get(
  routeMap.Naver + routeMap.Callback,
  OnlyForNotAuthorized,
  passport.authenticate("naver", naverAuthenticateCallBack)
);

module.exports = userRouter;
