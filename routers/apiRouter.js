const express = require("express");
const routeMap = require("../routeMap");
const {
  postWriteComment,
  getCommentList
} = require("../controller/commentController");
const {
  getFileDownload,
  postGetWeather,
  getTranslateText
} = require("../controller/globalController");

const apiRouter = express.Router();

apiRouter.get(routeMap.Comment, getCommentList);
apiRouter.post(routeMap.WriteComment, postWriteComment);

apiRouter.get(routeMap.Download(), getFileDownload);

apiRouter.post(routeMap.Weather, postGetWeather);

apiRouter.get(routeMap.Translate, getTranslateText);

module.exports = apiRouter;
